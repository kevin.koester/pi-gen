#!/bin/bash -e

if [ ! -d "${ROOTFS_DIR}" ]; then
	copy_previous
fi

rm "${ROOTFS_DIR}/etc/default/hostapd" > /dev/null || true # fix bug on rebuild
