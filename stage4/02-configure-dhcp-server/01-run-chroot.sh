#!/bin/bash -ex

if [ -z ${WLAN_PASSWORD+x} ]; then
 echo "Skipping AP config"
else

install -v -d							"${ROOTFS_DIR}/etc/hostapd"

cat <<EOF > /etc/hostapd/hostapd.conf
interface=${WLAN_INTERFACE}
driver=nl80211
ssid=rpi-cluser-master
channel=1

# ESSID sichtbar
ignore_broadcast_ssid=0

# Ländereinstellungen
country_code=DE
ieee80211d=1

# draft-n
ieee80211n=1

# Übertragungsmodus
hw_mode=g

# 40mhz kanalbreite
#ht_capab=[HT40+][SHORT-GI-40][DSSS_CCK-40]

# MAC-Authentifizierung
macaddr_acl=0

# wmm-Funktionalität
wmm_enabled=0

# WLAN-Verschlüsselung
auth_algs=1
wpa=2
wpa_key_mgmt=WPA-PSK
rsn_pairwise=CCMP
wpa_passphrase=${WLAN_PASSWORD}

EOF

chmod 600 /etc/hostapd/hostapd.conf

cat <<EOF > /etc/default/hostapd
DAEMON_CONF="/etc/hostapd/hostapd.conf"

EOF

systemctl enable hostapd

cat <<EOF >> /etc/dhcpcd.conf
interface ${WLAN_INTERFACE}
static ip_address=192.168.2.1/24
static ip6_address=fd12::1/48
EOF

fi # END AP

cat <<EOF > /etc/sysctl.d/00-router.conf
net.ipv6.conf.all.forwarding=1
net.ipv4.ip_forward = 1
EOF

# Firewall
cat <<EOF > /etc/iptables.rules
# Allow all forwards from local network. no masquerade needed
iptables -A FORWARD -i wlan0 -j ACCEPT
iptables -A FORWARD -i eth0 -j ACCEPT

ip6tables -A FORWARD -i wlan0 -j ACCEPT
ip6tables -A FORWARD -i eth0 -j ACCEPT
#TODO: add modem

EOF

chdmod 755 /etc/iptables.rules

sed -i '$i/etc/iptables.rules' /etc/rc.local

cat <<EOF >> /etc/dhcpcd.conf
interface ${LAN_INTERFACE}
static ip_address=192.168.1.1/24
static ip6_address=fd11::1/48
EOF

cat <<EOF > /etc/dnsmasq.conf
domain-needed
bogus-priv
localise-queries
dhcp-authoritative
expand-hosts
read-ethers
local-service

#for local hostnames
#addn-hosts=/tmp/hosts

local=/local/
domain=local

server=134.28.202.14

enable-ra
ra-param=high,60
dhcp-range=interface:eth0, ::, constructor:eth0, ra-stateless
dhcp-range=interface:eth0,192.168.1.100,192.168.1.150,12h

dhcp-range=interface:wlan0, ::, constructor:wlan0, ra-stateless
dhcp-range=interface:wlan0,192.168.2.100,192.168.2.150,12h

dhcp-option=eth0,3,192.168.1.1
dhcp-option=wlan0,3,192.168.2.1

dhcp-host=b8:27:eb:2f:33:b3,192.168.1.101
dhcp-host=b8:27:eb:1e:5b:13,192.168.1.105
dhcp-host=b8:27:eb:29:5b:7a,192.168.1.102
dhcp-host=b8:27:eb:7f:0b:9d,192.168.1.103
dhcp-host=b8:27:eb:6c:d3:99,192.168.1.104

EOF

