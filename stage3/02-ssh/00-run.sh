#!/bin/bash -ex

install -v -d							"${ROOTFS_DIR}/root/.ssh"
install -v -m 644 files/keys			"${ROOTFS_DIR}/root/.ssh/authorized_keys"

echo "" > "${ROOTFS_DIR}/boot/ssh"

on_chroot<<EOF

# Disable password authentication
grep -q "ChallengeResponseAuthentication" /etc/ssh/sshd_config && sed -i "/^[^#]*ChallengeResponseAuthentication[[:space:]]yes.*/c\ChallengeResponseAuthentication no" /etc/ssh/sshd_config || echo "ChallengeResponseAuthentication no" >> /etc/ssh/sshd_config
grep -q "^[^#]*PasswordAuthentication" /etc/ssh/sshd_config && sed -i "/^[^#]*PasswordAuthentication[[:space:]]yes/c\PasswordAuthentication no" /etc/ssh/sshd_config || echo "PasswordAuthentication no" >> /etc/ssh/sshd_config

ls /root/.ssh
EOF

