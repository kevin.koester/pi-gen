#!/bin/bash -ex

install -v -d							"${ROOTFS_DIR}/usr/share/ca-certificates/extra"
install -v -m 644 files/ca-root.pem		"${ROOTFS_DIR}/usr/share/ca-certificates/extra/"

on_chroot << EOF
echo "extra/ca-root.pem" > /etc/ca-certificates.conf
update-ca-certificates
EOF
