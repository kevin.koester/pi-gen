#!/bin/bash -ex

GOPATH=/tmp/go
BINARY=$GOPATH/bin/registry

DOCKER_REGISTRY_USER=$1
DOCKER_REGISTRY_PASSWORD=$2

echo "Building registry..."
CGO_ENABLED=0 GOOS=linux GOARCH=arm GOARM=5 go get -a -ldflags '-s' github.com/docker/distribution/cmd/registry
cp /tmp/go/bin/linux_arm/registry ./
echo "Building docker image..."
sudo docker build -t registry . -f -<< EOF 
from scratch

COPY registry /registry
COPY config.yml /config.yml

VOLUME ["/data"]
EXPOSE 5000

ENTRYPOINT ["/registry"]
CMD ["serve", "/config.yml"]
EOF

rm registry

mkdir -p stage4/03-install-docker-registry/files/registry
sudo docker save registry -o stage4/03-install-docker-registry/files/registry/registry.tar
sudo docker rmi registry

mkdir -p stage4/03-install-docker-registry/files/registry/auth/
htpasswd -Bbn ${DOCKER_REGISTRY_USER} ${DOCKER_REGISTRY_PASSWORD} >> "stage4/03-install-docker-registry/files/registry/auth/htpasswd"

echo "Done!"

