#!/bin/bash -x

SUBJECT_ROOT_COUNTRY="DE" # 2 letter code
SUBJECT_ROOT_STATE="Germany" # 
SUBJECT_ROOT_LOCALITY="Hamburg" # city
SUBJECT_ROOT_ORGANIZATION="Universität Hamburg"
SUBJECT_ROOT_ORGANIZATIONAL_UNIT="SVS RPI cluster" # section
SUBJECT_ROOT_COMMON_NAME="CA CERT" # URL!!!
SUBJECT_ROOT_MAIL="0koester@informatik.uni-hamburg.de"


SUBJECT_COUNTRY="DE" # 2 letter code
SUBJECT_STATE="Germany" # 
SUBJECT_LOCALITY="Hamburg" # city
SUBJECT_ORGANIZATION="Universität Hamburg"
SUBJECT_ORGANIZATIONAL_UNIT="SVS RPI cluster" # section
SUBJECT_COMMON_NAME="master-node.local" # URL!!!
SUBJECT_MAIL="0koester@informatik.uni-hamburg.de"

openssl genrsa -out ca-key.pem 4096
openssl req -x509 -new -nodes -extensions v3_ca -key ca-key.pem -days 1825 -out ca-root.pem -sha512 -subj "/C=${SUBJECT_ROOT_COUNTRY}/ST=${SUBJECT_ROOT_STATE}/L=${SUBJECT_ROOT_LOCALITY}/O=${SUBJECT_ROOT_ORGANIZATION}/OU=${SUBJECT_ROOT_ORGANIZATIONAL_UNIT}/CN=${SUBJECT_ROOT_COMMON_NAME}/emailAddress=${SUBJECT_ROOT_MAIL}"
openssl genrsa -out certificate-key.pem 4096
openssl req -new -key certificate-key.pem -out certificate.csr -sha512 -subj "/C=${SUBJECT_COUNTRY}/ST=${SUBJECT_STATE}/L=${SUBJECT_LOCALITY}/O=${SUBJECT_ORGANIZATION}/OU=${SUBJECT_ORGANIZATIONAL_UNIT}/CN=${SUBJECT_COMMON_NAME}/emailAddress=${SUBJECT_MAIL}"
openssl x509 -req -in certificate.csr -CA ca-root.pem -CAkey ca-key.pem -CAcreateserial -out certificate.pem -days 365 -sha512

cp certificate-key.pem stage4/03-install-docker-registry/files/registry/certs/registry-key.pem
cp certificate.pem stage4/03-install-docker-registry/files/registry/certs/registry.pem

cp ca-root.pem stage3/03-add-certs/files/

rm certificate.csr
